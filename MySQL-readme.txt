see: generate_scheqa.sql

#!/bin/bash                                                                                                                                                                       
csvsql --dialect mysql --snifflimit 100000 detailed_strategies.csv > detailed_strategies.sql
csvsql --dialect mysql --snifflimit 100000 soa.csv > soa.sql







SET SQL_SAFE_UPDATES = 0;
DELETE FROM metabase_demo.detailed_strategies;
DELETE FROM metabase_demo.soa;

SELECT * FROM metabase_demo.detailed_strategies;

LOAD DATA LOCAL INFILE '/Users/sohan/Desktop/metabase/detailed_strategies_data.csv'
INTO TABLE metabase_demo.detailed_strategies
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';


LOAD DATA LOCAL INFILE '/Users/sohan/Desktop/metabase/soa_data.csv'
INTO TABLE metabase_demo.soa
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';


-- u/n: metabase_demo
-- p/w: nu10834unsv8

GRANT SELECT ON detailed_strategies TO `metabase_demo` IDENTIFIED BY 'nu10834unsv8';


UPDATE soa 
SET 
    dealer_group = 'Capital'
WHERE
    dealer_group = 'Elders';

UPDATE soa 
SET 
    dealer_group = 'Group'
WHERE
    dealer_group = 'Shadforth';

UPDATE soa 
SET 
    dealer_group = 'Hillross'
WHERE
    dealer_group = 'Lonsdale';


UPDATE soa 
SET 
    dealer_group = 'Hillross'
WHERE
    dealer_group = 'FSP';
