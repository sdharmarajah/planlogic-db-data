from strategies import strategies_data_dict
from helpers import random_date, random_finish_date
import random
import numpy as np
import math
from pprint import pprint

class Soa(object):
    def __init__(self, advisor, soa_id, param1, param2, param3, start_time, end_time):
        # param1: what % of warnings translate to "red alert custom warnings" as per Shehan's note

        # An SoA object contains:
        #
        # SoA statistics:
        # - dealer group,
        # - advisor,
        # - SoA start time,
        # - SoA end time,
        # - time spent: on Current/Strategy/Investment/Insurance/SoA/Presentation
        # - client data: age, income, married/single
        #
        # Warning statistics:
        # - # high/medium/low warnings,
        # - # warnings not addressed: high/medium/low warnings (corresponding to point above)
        #
        # "Detailed" strategy statistics:
        # - strategies recommended by high and low level: Superannuation, Salary Sacrifice

        self.soa_header_row = list()
        self.soa_csv = list()

        # dealer, advisor and duration data
        self.soa_id = soa_id
        self.dealer_group = advisor.dealer_group
        self.advisor = advisor.full_name
        # generate random start and completion dates
        min_soa_days = 2
        max_soa_days = 8
        self.soa_start_time = random_date(start_time, end_time, random.random())
        self.soa_end_time = random_finish_date(self.soa_start_time, min_soa_days, max_soa_days, random.random())
        # set header and csv data
        self.soa_header_row.extend(['dealer_group', 'advisor', 'soa_id', 'soa_start_time','soa_end_time'])
        self.soa_csv.extend([self.dealer_group, self.advisor, self.soa_id, self.soa_start_time, self.soa_end_time])
        

        # timing statistics
        self.time_current = random.randint(5,15)
        self.time_investment = random.randint(5,15)
        self.time_insurance = random.randint(5,15)
        self.time_soa = random.randint(5,15)
        self.time_presentation = random.randint(10,45)
        # set header and csv data
        self.soa_header_row.extend(['time_current', 'time_investment', 'time_insurance','time_soa', 'time_presentation'])
        self.soa_csv.extend([self.time_current, self.time_investment, self.time_insurance, self.time_soa, self.time_presentation])

        # client data
        self.client_age = random.randint(45,65)
        self.client_income = 1000*random.randint(40,120)
        # set header and csv data
        self.soa_header_row.extend(['client_age', 'client_income'])
        self.soa_csv.extend([self.client_age, self.client_income])

        # warning severity
        self.warnings_low = random.randint(0,5)
        self.warnings_medium = random.randint(0,4)
        self.warnings_high = random.randint(0,3)
        # set header and csv data
        self.soa_header_row.extend(['Low_issues_raised', 'Medium_issues_raised', 'High_issues_raised'])
        self.soa_csv.extend([self.warnings_low, self.warnings_medium, self.warnings_high])

        # warnings addressed
        self.warnings_addressed_low = self.warnings_low - random.randint(0,self.warnings_low)
        self.warnings_addressed_medium = self.warnings_medium - random.randint(0,self.warnings_medium)
        self.warnings_addressed_high = self.warnings_high - random.randint(0,self.warnings_high)
        # set header and csv data
        self.soa_header_row.extend(['Low_issues_overridden', 'Medium_issues_overridden', 'High_issues_overridden'])
        self.soa_csv.extend([self.warnings_addressed_low, self.warnings_addressed_medium, self.warnings_addressed_high])

        # warnings by section
        total_warnings = self.warnings_low + self.warnings_medium + self.warnings_high
        weights = np.random.random(4)
        # weights = np.random.normal(size=4)
        weights /= weights.sum()
        self.warnings_strategy = math.floor(weights[0] * total_warnings)
        self.warnings_investment = math.floor(weights[1] * total_warnings)
        self.warnings_insurance = math.floor(weights[2] * total_warnings)
        self.warnings_soa = total_warnings - (self.warnings_strategy + self.warnings_investment + self.warnings_insurance)
        # set header and csv data
        self.soa_header_row.extend(['issues_strategy', 'issues_investment', 'issues_insurance','issues_soa'])
        self.soa_csv.extend([self.warnings_strategy, self.warnings_investment, self.warnings_insurance, self.warnings_soa])

        # custom warnings from Shehan:
        # - warning_custom_asset_allocation: portfolio asset allocation not in line with risk profile
        # - warning_custom_expensive: recommended investment product more expensive than current and alternative in ROP tables
        # - warning_custom_goals: client goals not addressed (goals unlinked to strategies)
        # - warning_custom_worse_assets: client recommended net asset position worse than current
        self.warning_custom_asset_allocation = math.floor(param1*random.randint(0,self.warnings_investment))
        self.warning_custom_expensive = math.floor(param1*random.randint(0,self.warnings_investment))
        self.warning_custom_goals = math.floor(param1*random.randint(0,self.warnings_soa))
        self.warning_custom_worse_assets = math.floor(param1*random.randint(0,self.warnings_soa))
        # set header and csv data
        self.soa_header_row.extend(['Issues_custom_asset_allocation', 'Issues_custom_expensive', 'Issues_custom_goals', 'Issues_custom_worse_assets'])
        self.soa_csv.extend([self.warning_custom_asset_allocation, self.warning_custom_expensive, self.warning_custom_goals, self.warning_custom_worse_assets])

    def get_soa_header_row(self):
        return ",".join([str(item) for item in self.soa_header_row])

    def get_soa_csv(self, is_for_schema=False):
        if is_for_schema is True:
            num_cols = len(self.soa_csv)
            for i in range(5,num_cols):
                self.soa_csv[i] = 99
        return ",".join([str(item) for item in self.soa_csv])

    def get_detailed_strategies_header_row(self):
        column_names = list()
        column_names.extend(['dealer_group', 'advisor','soa_id', 'soa_start_time','soa_end_time'])
        for strategy_section in strategies_data_dict:
            column_names.append(strategy_section)
            column_names.extend(strategies_data_dict[strategy_section])
        return ",".join(column_names)

    def get_detailed_strategies_csv(self, is_for_schema=False):
        column_values = list()
        column_values.extend([self.dealer_group, self.advisor, self.soa_id, self.soa_start_time, self.soa_end_time])
        for strategy_section in strategies_data_dict:
            # recommending 1-4 strategies for each section
            total_strategies = random.randint(1,4)
            # sohan - for generating SQL schema
            if is_for_schema is True:
                total_strategies = 4

            # calculate values for this strategy section (high level, detailed1, detailed2, etc)
            strategy_section_values = list()
            strategy_section_values.append(total_strategies)
            num_strategy_detailed_section = len(strategies_data_dict[strategy_section])
            weights = np.random.random(num_strategy_detailed_section)
            # weights = np.random.normal(size=4)
            weights /= weights.sum()
            # pprint(weights)


            # detailed_strategies_values are the values for the detailed strategies
            # we keep this separately, so it's easier to handle the "math.flooring" issue for the last entry
            detailed_strategies_values = list()
            for i, strategy_detailed_section in enumerate(strategies_data_dict[strategy_section]):
                num_detailed_strategies = math.floor(weights[i] * total_strategies)
                detailed_strategies_values.append(num_detailed_strategies)
                
            # correct last column, in case of math.flooring error
            sum_except_last = sum(detailed_strategies_values[0:len(detailed_strategies_values)-1])
            detailed_strategies_values[len(detailed_strategies_values)-1] = max(0,total_strategies - sum_except_last)
            strategy_section_values.extend(detailed_strategies_values)
            # concat detailed_strategies_values to column_values
            # sohan - for generating SQL schema
            if is_for_schema is True:
                for i in range(0, len(strategy_section_values)):
                    strategy_section_values[i] = 9

            column_values.extend(strategy_section_values)

        return ",".join([str(item) for item in column_values])

class Advisor(object):
    def __init__(self, full_name, dealer_group):
        self.full_name = full_name
        self.dealer_group = dealer_group

        # set scaling parameters
        self.param1 = 0.3
        self.param2 = 1.0
        self.param3 = 1.0

    def generate_soa(self, soa_id, start_time, end_time):
        # generate: "SoA statistics" and "Warnings statistics"
        soa = Soa(self, soa_id=soa_id, param1=self.param1, param2=self.param2, param3=self.param3, start_time=start_time, end_time=end_time)
        return soa