import time

# helper functions
# https://stackoverflow.com/questions/553303/generate-a-random-date-between-two-other-dates
def str_time_prop(start, end, format, prop):
    """Get a time at a proportion of a range of two formatted times.
    start and end should be strings specifying times formated in the
    given format (strftime-style), giving an interval [start, end].
    prop specifies how a proportion of the interval to be taken after
    start.  The returned time will be in the specified format.
    """

    # resulting time should be 9:00 AM <= t <= 6:00 PM
    stime = time.mktime(time.strptime(start, format))
    etime = time.mktime(time.strptime(end, format))
    ptime = stime + prop * (etime - stime)
    hour = int(time.strftime('%H', time.localtime(ptime)))
    # t = time.strftime('%m/%d/%Y %I:%M %p', time.localtime(ptime))

    if hour < 9:
        diff = 9 - hour
        ptime += diff*60*60
    elif hour > 17:
        diff = hour - 17
        ptime -= diff * 60 * 60
    # hour = time.strftime('%H', time.localtime(ptime))
    # t = time.strftime('%m/%d/%Y %I:%M %p', time.localtime(ptime))
    return time.strftime(format, time.localtime(ptime))

# helper functions
# https://stackoverflow.com/questions/553303/generate-a-random-date-between-two-other-dates
def random_finish_date(start, min_days, max_days, prop):

    # resulting time should be 9:00 AM <= t <= 6:00 PM
    stime = time.mktime(time.strptime(start, '%Y-%m-%d %H:%M:%S'))
    ptime = stime + (min_days + prop*max_days)*24*60*60
    hour = int(time.strftime('%H', time.localtime(ptime)))
    # t = time.strftime('%m/%d/%Y %I:%M %p', time.localtime(ptime))

    if hour < 9:
        diff = 9 - hour
        ptime += diff*60*60
    elif hour > 17:
        diff = hour - 17
        ptime -= diff * 60 * 60
    # hour = time.strftime('%H', time.localtime(ptime))
    # t = time.strftime('%m/%d/%Y %I:%M %p', time.localtime(ptime))
    return time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(ptime))

def random_date(start, end, prop):
    # return str_time_prop(start, end, '%m/%d/%Y %I:%M %p', prop)
    return str_time_prop(start, end, '%Y-%m-%d %H:%M:%S', prop)