# from src/enums/strategyChoices.ts

strategies_data_dict = dict()

# superannuation strategies
strategies_superannuation = dict()
superannuation_list = ['Salary Sacrifice', 'Non-concessional contribution', 'Personal deductible contributions', 'Spouse contributions']
strategies_data_dict.update({'Superannuation': superannuation_list})

# pension strategies
strategies_pension = dict()
pension_list = ['Commence account based pension', 'Commence transition to retirement pension', 'Custom Pension Strategy']
strategies_data_dict.update({'Pension': pension_list})

# investments strategies
strategies_investment = dict()
investment_list = ['Establish a new investment portfolio', 'Add funds into existing investment portfolio', 'Withdraw funds from investment portfolio', 'Custom Investment Strategy']
strategies_data_dict.update({'Investment': investment_list})

# debt strategies
strategies_debt = dict()
debt_list = ['Pay down loan', 'Reduce loan repayments', 'Custom Debt Strategy']
strategies_data_dict.update({'Debt': debt_list})

# Centrelink strategies
strategies_centrelink = dict()
centrelink_list = ['Apply for Centrelink payment', 'Gifting strategy', 'Centrelink Funeral Bond', 'Custom Centrelink Strategy']
strategies_data_dict.update({'Centrelink': centrelink_list})

# Insurance strategies
strategies_insurance = dict()
insurance_list = ['Establish', 'Retain', 'Vary', 'Cancel']
strategies_data_dict.update({'Insurance': insurance_list})

# EstatePlanning strategies
strategies_estate_planning = dict()
estate_planning_list = ['Power of Attorney', 'Will', 'Superannuation Beneficiary', 'Estate Planning Funeral Bond', 'Implement an Advance Care', 'Change pension beneficiary nomination']
strategies_data_dict.update({'Estate Planning': estate_planning_list})






