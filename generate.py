import sys
import os.path
import random
from advisor import Advisor
from pprint import pprint
import csv
import numpy as np
import math
from helpers import random_date

current_path = os.path.abspath(os.path.dirname(__file__))
path = os.path.join(current_path, "..")
sys.path.append(path)

# delete all CSV files in current directoy
filelist = [ f for f in os.listdir(current_path) if f.endswith(".csv") ]
for f in filelist:
    os.remove(os.path.join(current_path, f))


# this script generates a single CSV with:
#  - "SoA statistics", and
#  - "Warnings statistics"
#
# SoA statistics:
# - dealer group,
# - advisor,
# - SoA start time,
# - SoA end time,
# - time spent: on Current/Strategy/Investment/Insurance/SoA/Presentation
# - client data: age, income, married/single
#
# Warning statistics:
# - # high/medium/low warnings,
# - # warning snot addressed: high/medium/low warnings (corresponding to point above)


# create advisor data
advisors = ['Lona Shireman', 'Gabriela Millican', 'Kary Turvey', 'Frederica Cotton', 'Lenita Don', 'Numbers Ebright', 'Laine Lilly', 'Mckinley Boehman', 'Catrice Peper', 'Pamala Favorite', 'Vernie Paradis', 'Jannie Pitzen', 'Rebecca Duplessis', 'Carlota Swindler', 'Mechelle Gines', 'Willis Sambrano', 'Una Vachon', 'Robbi Brosius', 'Hanh Salmons', 'Charita Penn', 'Ute Macneil', 'Daphine Hough', 'Dorinda Humber', 'Asley Bennett', 'Yvonne Rossin', 'Maximina Wear', 'Amado Burkitt', 'Tai Rux', 'Elaina Mabee', 'Pete Bermeo', 'Lashon Fritz', 'Benedict Rost', 'Michael Newquist', 'Kiersten Luongo', 'Gabriele Housley', 'Garry Stucky', 'Grayce Mulroy', 'Adriene Buentello', 'Caterina Tyree', 'Kittie Avallone', 'Ivana Ferrel', 'Fletcher Milera', 'Ellan Groman', 'Dionna Holts', 'Marquita Oldham', 'Bella Steinman', 'Edelmira Jeanes', 'Elois Tagle', 'Daine Frizzell', 'Natalya Pricer', 'Chae Semon', 'Terrilyn Zumwalt', 'Tayna Kreidler', 'Rhonda Wiechmann', 'Regenia Register', 'Collette Stock', 'Bettina Mor', 'Porsche Kuntz', 'Eldora Raab', 'Haydee Schaffer', 'Elissa Montes', 'Jetta Wherry', 'Claribel Urban', 'Olympia Burford', 'Maybelle Nehls', 'Norene Gano', 'Sue Melcher', 'Ty Janousek', 'Sanjuanita Nicastro', 'Adelle Ceaser', 'Kathlene Wiggins', 'Junko Kosak', 'Lyla Dahmen', 'Raleigh Coca', 'Hayden Morillo', 'Virgil Mani', 'Nolan Hummell', 'Judson Donatelli', 'Bernie Lemonds', 'Mattie Costales', 'Aida Zucco', 'Francina Grippo', 'Rochel Styer', 'Maryjo Zoeller', 'Jacquline Tibbitts', 'Tanya Doman', 'Clint Kash', 'Norah Frederickson', 'Vernon Freitas', 'Yasmin Miranda', 'Tien Courville', 'Bebe Wylie', 'Brook Bridgeman', 'Louvenia Rhee', 'Carlotta Sytsma', 'Grazyna Moreno', 'Stacy Palencia', 'Neal Sharer', 'Nakesha Corp', 'Iesha Legleiter', 'Margret Kesten', 'Katharine Perrier', 'Saundra Grinder', 'Leanora Kelly', 'Sharee Rutter', 'Pamela Elizondo', 'Detra Payson', 'Breann Weatherholtz', 'Moon Seybold', 'Leo Trump', 'Lanette Diggins', 'Mariette Combs', 'Teri Merry', 'Genna Kadlec', 'Demetrice Trumbo', 'Matilda Mcmiller', 'Sherwood Schutte', 'Denis Degnan', 'Sandee Gavin', 'Abraham Cotta', 'Tawanda Gilreath', 'Joel Pawlowicz', 'Sophie Wroten', 'Libbie Dries', 'Christie Gremillion']
# advisors = ['Jacquelyn Mansfield', 'Lona Shireman', 'Gabriela Millican', 'Kary Turvey', 'Frederica Cotton', 'Lenita Don', 'Numbers Ebright', 'Laine Lilly', 'Mckinley Boehman', 'Catrice Peper', 'Pamala Favorite', 'Vernie Paradis', 'Jannie Pitzen', 'Rebecca Duplessis', 'Carlota Swindler', 'Mechelle Gines', 'Willis Sambrano', 'Una Vachon', 'Robbi Brosius', 'Hanh Salmons', 'Charita Penn', 'Ute Macneil', 'Daphine Hough', 'Dorinda Humber', 'Asley Bennett', 'Yvonne Rossin', 'Maximina Wear', 'Amado Burkitt', 'Tai Rux', 'Elaina Mabee', 'Pete Bermeo', 'Lashon Fritz', 'Benedict Rost', 'Michael Newquist', 'Kiersten Luongo', 'Gabriele Housley', 'Garry Stucky', 'Grayce Mulroy', 'Adriene Buentello', 'Caterina Tyree', 'Kittie Avallone', 'Ivana Ferrel', 'Fletcher Milera', 'Ellan Groman', 'Dionna Holts', 'Marquita Oldham', 'Bella Steinman', 'Edelmira Jeanes', 'Elois Tagle', 'Daine Frizzell', 'Natalya Pricer', 'Chae Semon', 'Terrilyn Zumwalt', 'Tayna Kreidler', 'Rhonda Wiechmann', 'Regenia Register', 'Collette Stock', 'Bettina Mor', 'Porsche Kuntz', 'Eldora Raab', 'Haydee Schaffer', 'Elissa Montes', 'Jetta Wherry', 'Claribel Urban', 'Olympia Burford', 'Maybelle Nehls', 'Norene Gano', 'Sue Melcher', 'Ty Janousek', 'Sanjuanita Nicastro', 'Adelle Ceaser', 'Kathlene Wiggins', 'Junko Kosak', 'Lyla Dahmen', 'Raleigh Coca', 'Hayden Morillo', 'Virgil Mani', 'Nolan Hummell', 'Judson Donatelli', 'Bernie Lemonds', 'Mattie Costales', 'Aida Zucco', 'Francina Grippo', 'Rochel Styer', 'Maryjo Zoeller', 'Jacquline Tibbitts', 'Tanya Doman', 'Clint Kash', 'Norah Frederickson', 'Vernon Freitas', 'Yasmin Miranda', 'Tien Courville', 'Bebe Wylie', 'Brook Bridgeman', 'Louvenia Rhee', 'Carlotta Sytsma', 'Grazyna Moreno', 'Stacy Palencia', 'Neal Sharer', 'Nakesha Corp', 'Iesha Legleiter', 'Margret Kesten', 'Katharine Perrier', 'Saundra Grinder', 'Leanora Kelly', 'Sharee Rutter', 'Pamela Elizondo', 'Detra Payson', 'Breann Weatherholtz', 'Moon Seybold', 'Leo Trump', 'Lanette Diggins', 'Mariette Combs', 'Teri Merry', 'Genna Kadlec', 'Demetrice Trumbo', 'Matilda Mcmiller', 'Sherwood Schutte', 'Denis Degnan', 'Sandee Gavin', 'Abraham Cotta', 'Tawanda Gilreath', 'Joel Pawlowicz', 'Sophie Wroten', 'Libbie Dries', 'Christie Gremillion', 'Janise Durden', 'Margart Stults', 'Ardis Fajardo', 'Waneta Ottinger', 'Malisa Mcnamee', 'Noreen Naab', 'Patrice Gause', 'Chelsey Viles', 'Kia Dumont', 'Neta Thornberry', 'Jesica Aslett', 'Jacqulyn Dantin', 'Kymberly Murakami', 'Winfred Funchess', 'Alpha Rentz', 'Leticia Hopwood', 'Neely Bober', 'Marlin Earwood', 'Camille Hinkle', 'Cari Bodiford', 'Ehtel Tsan', 'Aurora Tan', 'Flossie Donaldson', 'Kary Meaders', 'Moises Hilson', 'Contessa Cueva', 'Gema Bezanson', 'Florrie Grate', 'Shondra Dora', 'Torri Artz', 'Jacob Kinner', 'Jetta Carmon', 'Bruna Bold', 'Rocco Damico', 'Carson Noffsinger', 'Porsha Warkentin', 'Tyron Max', 'Kassandra Franz', 'Johanne Weick', 'Danial Meller', 'Genevieve Wilkey', 'Angelika Orton', 'Vania Musich', 'Jone Richey', 'Lorraine Dunleavy', 'Rosalina Raye', 'Inge Glueck', 'Eliana Fikes', 'Hunter Okelly', 'Terese Mcleish', 'Yadira Singleton', 'Fernanda Nocera', 'Sheryl Hajduk', 'Lurline Maldonado', 'Oralee Krull', 'Ava Running', 'Ginette Revel', 'Gaynelle Neuendorf', 'Russell Chapel', 'Kaitlyn Sprenger', 'Magnolia Twine', 'Lashawn Holtzman', 'Chance Knaub', 'Elli Krehbiel', 'Andree Benn', 'Kacy Cerna', 'Augusta Ratcliff', 'Florentina Jarmon', 'Madalene Mcelwee', 'Taren Lasher', 'Shyla Portillo', 'Zofia Rames', 'Hyman Stella', 'Sharmaine Gall']
# advisors = ['Jacquelyn Mansfield', 'Lona Shireman', 'Gabriela Millican']
num_advisors = len(advisors)

# create  dealer group data
dealer_groups = ['RI', 'FSP', 'Millennium3', 'Elders', 'Lonsdale', 'Shadforth']
num_dealer_groups = len(dealer_groups)

# set params
# %Y-%m-%d %H:%M:%S
max_SoAs_per_advisor = 40
# SoA_range_start = "1/1/2008 1:30 PM"
# SoA_range_end = "1/1/2009 4:50 AM"
# yyyy-mm-dd
SoA_range_start = "2019-1-1 08:30:00"
SoA_range_end = "2019-12-31 17:30:00"

pprint("Generating data for: " + str(num_advisors) + " advisors, " + str(num_dealer_groups) + " dealer groups.")

with open('detailed_strategies_data.csv', 'a') as detailed_strategies_csv_file:
    detailed_strategies_writer = csv.writer(detailed_strategies_csv_file)
    with open('soa_data.csv', 'a') as soa_csv_file:
        soa_writer = csv.writer(soa_csv_file)

        # generate SoA
        soa_list = list()
        for i, advisor in enumerate(advisors):
            # loop 1: create (temp) advisor object
            tmp_advisor = Advisor(full_name=advisor, dealer_group=dealer_groups[random.randint(0,num_dealer_groups-1)])
            # generate SoAs for this advisor
            # num_soas = random.randint(6,max_SoAs_per_advisor)
            num_soas = math.ceil(np.random.normal(loc=30, scale=4))
            for j in range(0,num_soas):
                soa = tmp_advisor.generate_soa(soa_id=(i*num_advisors)+j, start_time=SoA_range_start, end_time=SoA_range_end)
                # append SoA to list
                soa_list.append(soa)

                pprint('******************************************************')
                pprint('detailed_strategies_header_row')
                pprint(soa.get_detailed_strategies_header_row())
                pprint('******************************************************')
                pprint('detailed_strategies_csv')
                pprint(soa.get_detailed_strategies_csv())
                pprint('******************************************************')
                pprint('soa_header_row')
                pprint(soa.get_soa_header_row())
                pprint('******************************************************')
                pprint('soa_csv')
                pprint(soa.get_soa_csv())

                # write data to CSV
                detailed_strategies_csv_file.write(soa.get_detailed_strategies_csv() + '\n')
                soa_csv_file.write(soa.get_soa_csv() + '\n')


                # if i == 0 and j == 0:
                #     detailed_strategies_csv_file.write(soa.get_detailed_strategies_header_row() + '\n')
                #     soa_csv_file.write(soa.get_soa_header_row() + '\n')
                # else:
                #     detailed_strategies_csv_file.write(soa.get_detailed_strategies_csv() + '\n')
                #     soa_csv_file.write(soa.get_soa_csv() + '\n')

    soa_csv_file.close()
detailed_strategies_csv_file.close()



# write the header rows (to generate SQL)
dummy_advisor = Advisor(full_name="xxxxxxxxxx xxxxxxxxxxxxxxxxxxxxx", dealer_group="xxxxxxxxxxxxxxxxxxxxx")
dummy_soa = dummy_advisor.generate_soa(soa_id=1234567, start_time=SoA_range_start, end_time=SoA_range_end)
with open('detailed_strategies.csv', 'a') as header_detailed_strategies:
    detailed_strategies_writer = csv.writer(detailed_strategies_csv_file)
    with open('soa.csv', 'a') as header_soa:
        soa_writer = csv.writer(soa_csv_file)

        # write header and 1 row
        header_detailed_strategies.write(dummy_soa.get_detailed_strategies_header_row() + '\n')
        header_detailed_strategies.write(dummy_soa.get_detailed_strategies_csv(is_for_schema=True) + '\n')

        header_soa.write(dummy_soa.get_soa_header_row() + '\n')
        header_soa.write(dummy_soa.get_soa_csv(is_for_schema=True) + '\n')

        header_soa.close()
header_detailed_strategies.close()

# # generate SoA
# soa_list = list()
# for i, advisor in enumerate(advisors):
#     # loop 1: create (temp) advisor object
#     tmp_advisor = Advisor(full_name=advisor, dealer_group=dealer_groups[0])
#     # generate SoAs for this advisor
#     num_soas = random.randint(0,max_SoAs_per_advisor)
#     for j in range(0,num_soas):
#         soa = tmp_advisor.generate_soa(start_time=SoA_range_start, end_time=SoA_range_end)
#         # append SoA to list
#         soa_list.append(soa)
#
#         pprint('******************************************************')
#         pprint('detailed_strategies_header_row')
#         pprint(soa.get_detailed_strategies_header_row())
#         pprint('******************************************************')
#         pprint('detailed_strategies_csv')
#         pprint(soa.get_detailed_strategies_csv())
#         pprint('******************************************************')
#         pprint('soa_header_row')
#         pprint(soa.get_soa_header_row())
#         pprint('******************************************************')
#         pprint('soa_csv')
#         pprint(soa.get_soa_csv())
